#include <stdio.h>
#include "eval_exp.h"
#define NCODE 100
#define NSTACK 100
union Code code[NCODE];
int stack[NSTACK];
char *funs[NCODE];
int stackp;
int pc;
int eval(struct Tree *t){
	pc = generate(0,t);
	printf("stackp=%d; pc=%d\n", stackp, pc);
	printf("in generate\n");
	int tpc=pc;
	pc = 0;
	while(pc<tpc){
		if(code[pc].op!=NULL && funs[pc]!=NULL){
			printf("funs is [%s]\n", funs[pc]);
		}else{ 
			if(code[pc].value<10000){
				printf("value is [%d]\n", code[pc].value);

			}else {
				printf("symbol is [%s]\n", code[pc].symbol->name);
			}
		}
		pc++;
	}
	pc=tpc;
	code[pc].op=NULL;
	stackp = 0;
	pc = 0;
	printf("stackp=%d; pc=%d\n", stackp, pc);
	while(code[pc].op != NULL){
		(*code[pc++].op)();
		printf("temp res=%d", stack[stackp-1]);
	}
	return stack[0];

}

int generate(int codep, struct Tree *t){
	switch(t->op){
	case NUMBER:
		code[codep++].op = pushop;
		funs[codep-1]="pushop";
		code[codep++].value = t->value;
		return codep;
	case VARIABLE:
		code[codep++].op = pushsymop;
		funs[codep-1]="pushsymop";
		code[codep++].symbol = t->symbol;
		return codep;
	case ADD:
		codep=generate(codep, t->left);
		codep=generate(codep, t->right);
		code[codep++].op=addop;
		funs[codep-1]="addop";
		return codep;
	case DIVIDE:
		codep=generate(codep, t->left);
		codep=generate(codep, t->right);
		code[codep++].op=divop;
		funs[codep-1]="divop";
		return codep;
	case MAX:
		codep=generate(codep, t->left);
		codep=generate(codep, t->right);
		code[codep++].op=maxop;
		funs[codep-1]="maxop";
		return codep;
	case ASSIGN:
	//	codep=generate(codep, t->left);
		codep=generate(codep, t->right);
		code[codep++].op = storesymop;
		funs[codep-1]="storesymop";
		code[codep++].symbol = t->left->symbol;	
		return codep;
	}
}

void storesymop(void){
	printf("in storesymop\n");
	code[pc++].symbol->value=stack[stackp-1];
}
void pushop(void){
	printf("in pushop\n");
	stack[stackp++] = code[pc++].value;
}
void pushsymop(void){
	printf("in pushsymop\n");
	stack[stackp++] = code[pc++].symbol->value;
}
void divop(void){
	printf("in divop\n");
	int left, right;
	right = stack[--stackp];
	left = stack[--stackp];
	if(right == 0){
		printf("divide by zero\n");
	}
	stack[stackp++] = left/right;
}
void maxop(void){
	printf("in maxop\n");
	int left, right;
	right = stack[--stackp];
	left = stack[--stackp];
	stack[stackp++] = left>right? left : right;
}
void addop(void){
	int left, right;
	right = stack[--stackp];
	left = stack[--stackp];
	stack[stackp++] = left+right;

}
void init_code(void){
	int i;
	for(i=0;i<NCODE;i++){
		code[i].op=0;
	}	
}
int main(int argc, char *args[]){
	init_test_data();	
	symbol[0].value=2;
        symbol[1].value=2;
	printf("init data\n");
	int res = eval(&tree[6]);
	printf("The result is a= %d\n", res);
	printf("The result is a= %d", symbol[0].value);
}

