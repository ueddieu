struct Symbol {
        int value;
        char *name;
};
enum{
        NUMBER,
        VARIABLE,
        ADD,
        DIVIDE,
        MAX,
        ASSIGN
};

struct Tree {
        int op;
        int value;
        struct Symbol *symbol;
        struct  Tree *left;
        struct Tree *right;
};

union Code {
        void (*op)(void);
        int value;
        struct Symbol *symbol;
};
void pushop(void);
void pushsymop(void);
void addop(void);
void divop(void);
void maxop(void);
void storesymop(void);
void init_test_data(void);
extern struct Tree tree[];
extern struct Symbol symbol[];

