#include <stdio.h>
#include "eval_exp.h"

int eval(struct Tree *t) {
	int left, right;
	switch (t->op) {
		case NUMBER:
			return t->value;
		case VARIABLE:
			return t->symbol->value;
		case ADD:
			return eval(t->left) + eval(t->right);
		case DIVIDE:
			left = eval(t->left);
			right = eval(t->right);
			if( right == 0) printf("divide %d by zero", left);
			return left/right;
		case MAX:	
			left = eval(t->left);
			right = eval(t->right);
			return left>right? left : right;
		case ASSIGN:
			t->left->symbol->value = eval(t->right);
			return t->left->symbol->value;
	}
}	

int main(int argc, char *args[]){
	
	init_test_data();
	symbol[0].value=2;
	symbol[1].value=2;

	int res = eval(&tree[6]);
	printf("The result is a= %d\n", res);
}

