#include <stdio.h>
#include "eval_exp.h"
struct Tree tree[7];
struct Symbol symbol[3];
void init_test_data(){
        symbol[0].name="c";
        symbol[1].name="b";
        symbol[2].name="a";

        tree[0].op=NUMBER;
        tree[0].value = 2;

        tree[1].op = VARIABLE;
        tree[1].symbol = &symbol[0];

        tree[2].op = DIVIDE;
        tree[2].left = &tree[1];
        tree[2].right= &tree[0];

        tree[3].op=VARIABLE;
        tree[3].symbol = &symbol[1];

        tree[4].op = MAX;
        tree[4].left = &tree[3];
        tree[4].right= &tree[2];

        tree[5].op=VARIABLE;
        tree[5].symbol = &symbol[2];

        tree[6].op = ASSIGN;
        tree[6].left = &tree[5];
        tree[6].right= &tree[4];
	
	return;
}
