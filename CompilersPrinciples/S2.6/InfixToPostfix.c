#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
void expr();
void term();
void error();
void match();
int lookahead;
void print(char s[]);
void printchar(char s);
int main(){
	lookahead = getchar();
	printchar(lookahead);
	expr();
	putchar('\n');
	return 0;
}

void expr(){
	term();
	while (1) {
		if (lookahead == '+'){
			match('+');
			term();
			putchar('+');
		} else if (lookahead == '-'){
			match('-');
			term();
			putchar('-');
		} else break;
	}
}

void term() {
	if (isdigit(lookahead)) {
		putchar(lookahead);
		match(lookahead);
	} else error();
}

void match(int t){
	if (lookahead == t) {
		lookahead = getchar();
	}else error();
}
void error() {
	fprintf(stderr, "syntax error\n");
	exit(1);
}
void print(char s[]) {
	//fprintf(stderr, "%s\n", s);
}
void printchar(char s) {
	//fprintf(stderr, "%c\n", s);
}
