#!/bin/sh
# $Id: //depot/Master/install-demo.sh#10 $  $Date: 2003/08/04 $  $Author: rahtz $
# 
# install-demo.sh -- install demo setup.  This is install-tl.sh
# in `demo' distributions.
# 
# Copyright (c) Thomas Esser, Sebastian Rahtz, 1996, 1997, 1998, 1999,
# 2000, 2001, 2003.
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
# 
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
# 
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#     
#    Send bug reports or suggestions to tex-demo@tug.org.
#
# Call this script with a full-featured bourne shell, which may be
#   /bin/sh, /bin/bsh (e.g. AIX-4.XX), /bin/sh5 (e.g. ULTRIX) or
#   /bin/bash (on GNU systems) on your system.


# set this for debugging...
debug=${OVERRIDE_DEBUG-false}

unset CDPATH  # avoid unwanted output

while test $# -gt 0; do
  case $1 in
    --debug) debug=true;;
    --cddir=*)
          cddir=`echo $1 | sed 's/.*=//'`;;
     *) break;;
  esac
  shift
done

HERE=`cd \`dirname $0\` && /bin/pwd`
. $HERE/utils.sh
. $HERE/common.sh

series_init()
{
  thisdir=`pwd`
  CDDIR=${cddir-${OVERRIDE_CDDIR-$thisdir}}
  LISTS=${OVERRIDE_LISTS-$CDDIR/texmf/tpm/lists}
  BIN=${OVERRIDE_LISTS-$CDDIR/bin}
  test -d $LISTS || fatal "$0: $LISTS: no such directory. Are you sure this is the TeX Demo CD?"

  $echon "Initializing collections... "
  setvars
  systems=`(cd $BIN; ls -l | grep "^d" | awk '{print $9}')`

  all_schemes=`(cd $LISTS; ls *.scheme | sed -e 's/-/_/' -e 's/\.scheme//' | sort )`
  sc=24
  for s in $all_schemes; do
    S=`echo $s | sed -e 's/_/-/' `
    sc=`expr $sc + 1`
    # *** xxx kludge for demo only; there is no texlive-full,
    # but we need to make the ident letters come out the same for the
    # sake of the doc.
    test $sc -eq 26 && sc=`expr $sc + 1`
    
    eval N=\"\$iden_${sc}\"
    eval SCHEMES_${N}=\"$s\"
    T=`grep '^\*Title' $LISTS/$S.scheme | sed -e 's/\*Title: //'`
    C=`grep '^\*Size'  $LISTS/$S.scheme | sed -e 's/\*Size: //'`
    eval schemes_${s}_n=\"$T\"
    eval schemes_${s}_ident=\"$N\"
    eval schemes_${s}_du=\"$C\"
  done

  all_lang_collections=`(cd $LISTS; ls *.vlist | grep tex-lang | sed 's/\.vlist//' | sed 's/-/_/' | sort )`
  sc=0
  for s in $all_lang_collections; do
    S=`echo $s | sed 's/_/-/'`
    sc=`expr $sc + 1`
    eval N=\"\$iden_${sc}\"
    eval LP_${N}=\"$s\"
    T=`grep '^\*Title' $LISTS/$S.vlist | sed -e 's/\*Title: //'`
    C=`grep '^\*Size'  $LISTS/$S.vlist | sed -e 's/\*Size: //'`
    eval p_${s}_n=\"$T\"
    eval p_${s}_ident=\"$N\"
    eval p_${s}_du=\"$C\"
    series_select_level $s 0
  done

  all_collections=`(cd $LISTS; ls *.vlist  | grep "tex-" | grep -v tex-lang | sed 's/\.vlist//' |  sed 's/-/_/' | sort )`
  sc=0
  for s in $all_collections; do
    S=`echo $s | sed 's/_/-/'`
    sc=`expr $sc + 1`
    eval N=\"\$iden_${sc}\"
    eval P_${N}=\"$s\"
    T=`grep '^\*Title' $LISTS/$S.vlist | sed -e 's/\*Title: //'`
    C=`grep '^\*Size'  $LISTS/$S.vlist | sed -e 's/\*Size: //'`
    eval p_${s}_n=\"$T\"
    eval p_${s}_ident=\"$N\"
    eval p_${s}_du=\"$C\"
    series_select_level $s 0
  done
  scheme_select texlive_recommended 
  selected_scheme=texlive_recommended
  echo "Done initializing collections."
  all_collections_anz=`echo $all_lang_collections $all_collections | awk '{print NF}'`
  systems_init
}


install_cd()
{
  TEXDIR=$CDDIR
  unset TEXMFCNF
  unset TEXMFMAIN
  TEXMFCNF_DIR=$TEXDIR/texmf/web2c
  echo "Preparing destination directories... " >&2
  test -d "$CDDIR" || fatal "$CDDIR does not seem to be the TeX Live Demo CD-ROM."
  alldirs="$VARTEXMF"
  test -z "$VARTEXMF" || alldirs="$alldirs $VARTEXMF/fonts/pk $VARTEXMF/fonts/tfm $VARTEXMF/web2c $VARTEXMF/pdftex/config $VARTEXMF/tex/generic/config $VARTEXMF/web2c $VARTEXMF/xdvi  $VARTEXMF/dvips/config  $VARTEXMF/etex/plain/config"
  for dir in $alldirs; do
    while test ! -d $dir || test ! -w $dir; do
      mkdirhier $dir
      test -d $dir || { warn "could not make directory '$dir'"; continue; }
      test -w $dir || { warn "cannot write to directory '$dir'"; continue; }
    done
  done
  test -z "$opt_varfonts_dir" ||
    chmod 1777 $opt_varfonts_dir/pk $opt_varfonts_dir/tfm ||
    warn "command 'chmod 1777 $opt_varfonts_dir/pk $opt_varfonts_dir/tfm' failed"

  pd=$this_platform_fn
  have_system=false
  if test -x $TEXDIR/bin/$pd/mktexlsr; then
    have_system=true
    bindir=$TEXDIR/bin/$pd
    PATH=$TEXDIR/bin/$pd:$PATH
    export PATH VARTEXMF
    echo system located as $pd
  fi
 
# copy over vartexmf from CD
 (cd $CDDIR/texmf-var; tar cf - .) | (cd $VARTEXMF; tar xf - )
 test -z "$VARTEXMF" || chmod -R a+w $VARTEXMF ||
     warn "command 'chmod -R a+w $VARTEXMF' failed"

# make TEXMFLOCAL tree
  make_local_skeleton

 $have_system && $bindir/mktexlsr
cat <<ENDC
    Now you are finished. 
    For future configuration, edit files in $VARTEXMF

    You should now add these setup statements to your environment,
    depending on what shell is running,
    
    For csh (~/.cshrc):

     setenv VARTEXMF $VARTEXMF
     setenv PATH $TEXDIR/bin/$pd:\$PATH

    For bash (~/.bash_profile) or sh (~/.profile):

     VARTEXMF=$VARTEXMF
     PATH=$TEXDIR/bin/$pd:\$PATH
     export PATH VARTEXMF
ENDC
  greetings
  exit
}

list_system_files()
{
  arg=$1
  :>$work_dir/$arg.list
  for i in $all_collections
  do
    I=`echo $i | sed 's/_/-/'`
    cat $LISTS/$I.vlist.$arg | grep -v '^ *$'  >>  $work_dir/$arg.list
  done
  for i in $all_lang_collections
  do
    I=`echo $i | sed 's/_/-/'`
    cat $LISTS/$I.vlist.$arg | grep -v '^ *$'  >>  $work_dir/$arg.list
  done
}


list_files()
{
  col=`echo $1 | sed 's/_/-/'`
  :>$work_dir/ftmp
  $debug && echo "         [ -> collection $col]" >&2
  filters="| grep -v '^ *$'  "
  if test "$opt_source" = X; then
     filters="$filters | grep -v texmf/source/"
  fi
  if test "$opt_doc" = X; then
     filters="$filters | grep -v texmf/doc/"
  fi
  if test -f $LISTS/$col.vlist; then
    packages=`grep "^+" $LISTS/$col.vlist | sed 's/.//'`
    morecols=`grep "^-" $LISTS/$col.vlist | sed 's/.//'`
    grep "^[a-z]" $LISTS/$col.vlist > $work_dir/ftmp
    for i in $all_systems
    do
      eval \$p_${i}_s || continue
      eval this=\$p_${i}_fn
      if test -f $LISTS/$col.vlist.$this; then
        $debug && echo "      system package $col.vlist.$this"  >&2
        cat $LISTS/$col.vlist.$this >> $work_dir/ftmp
      fi
    done
    for i in $packages
    do
      list_package $i $col
    done

    eval sort < $work_dir/ftmp $filters | grep -v "^\!" | uniq >> $work_dir/$col.list
    eval sort < $work_dir/ftmp | grep "^\!" | tr ' ' '=' >> $work_dir/$col.jobs
 fi
  rm $work_dir/ftmp

  for p in $morecols
  do
      list_files $p
  done

}

list_package()
{
  :>$work_dir/ptmp
  pack=`echo $1 | sed 's/_/-/'`
  col=$2
  filters="| grep -v '^ *$'  "
  if test "$opt_source" = X; then
     filters="$filters | grep -v texmf/source/"
  fi
  if test "$opt_doc" = X; then
     filters="$filters | grep -v texmf/doc/"
  fi
  $debug && echo "      package $pack"  >&2
      grep -v "^+" $LISTS/$pack >> $work_dir/ptmp
      for j in $all_systems
      do
      eval \$p_${j}_s || continue
        eval this=\$p_${j}_fn
        if test -f $LISTS/$pack.$this; then
          $debug && echo "        system package $pack.$this"  >&2
          cat $LISTS/$pack.$this >> $work_dir/ptmp
        fi
      done

    eval sort < $work_dir/ptmp $filters | grep -v "^\!" | uniq >> $work_dir/$col.list
    eval sort < $work_dir/ptmp | grep "^\!" | tr ' ' '=' >> $work_dir/$col.jobs
  rm $work_dir/ptmp
  for i in `grep "^+" $LISTS/$pack | sed 's/.//'`
  do
    list_package $i $col
  done

}


install_now()
{
  prepare_dirs
  make_local_skeleton
  mkdirhier $VARTEXMF/dvipdfm/config
  mkdirhier $VARTEXMF/dvips/config
  mkdirhier $VARTEXMF/web2c
cat > $VARTEXMF/web2c/updmap.cfg <<OAF
# updmap options
#
# Should dvips (by default) prefer bitmap fonts or outline fonts
# if both are available? Independend of this setting, outlines
# can be forced by putting "p psfonts_t1.map" into a config file
# that dvips reads. Bitmaps (for the fonts in question) can
# be forced by putting "p psfonts_pk.map" into a config file.
# We provide such config files which can be enabled via
# dvips -Poutline ... resp. dvips -Ppk ...
#
# Valid settings for dvipsPreferOutline are true / false:
dvipsPreferOutline true
#
# LW35
#
# Which fonts for the "Basic 35 Laserwriter Fonts" do you want to use and
# how are the filenames chosen? Valid settings:
#   URW:     URW fonts with "vendor" filenames (e.g. n019064l.pfb)
#   URWkb:   URW fonts with "berry" filenames (e.g. uhvbo8ac.pfb)
#   ADOBE:   Adobe fonts with "vendor" filenames (e.g. hvcbo___.pfb)
#   ADOBEkb: Adobe fonts with  "berry" filenames (e.g. phvbo8ac.pfb)
LW35 URWkb
#
# dvipsDownloadBase35
# 
# Should dvips (by default) download the standard 35 LaserWriter fonts
# with the document (then set dvipsDownloadBase35 true) or should these
# fonts be used from the ps interpreter / printer?
# Whetever the default is, the user can override it by specifying
# dvips -Pdownload35 ... resp. dvips -Pbuiltin35 ... to either download
# the LW35 fonts resp. use the built-in fonts.
#
# Valid settings are true / false:
dvipsDownloadBase35 false
#
# pdftexDownloadBase14
#
# Should pdftex download the base 14 pdf fonts? Since some configurations
# (ps / pdf tools / printers) use bad default fonts, it is safer to download
# the fonts. The pdf files will get bigger, though.
# Valid settings are true (download the fonts) or false (don't download
# the fonts).
pdftexDownloadBase14 true

################################################################
# Map files.
################################################################
#
# There are two possible entries: Map and MixedMap. Both have one additional
# argument: the filename of the map file. MixedMap ("mixed" means that
# the font is available as bitmap and as outline) lines will not be used
# in the default map of dvips if dvipsPreferOutline is false. Inactive
# Map files should be marked by "#! " (without the quotes), not just #.
#
OAF
  skip_systemstuff=false
  TEXMFCNF_DIR=$TEXDIR/texmf/web2c
  test -f $TEXMFCNF_DIR/texmf.cnf && skip_systemstuff=true
  (
    echo "Preparing list of files to be installed." >&2
    $debug && echo "For selected systems:" >&2
    for p in $all_systems; do
      eval \$p_${p}_s || continue
      eval name=\"\$p_${p}_n\"
      $debug && echo "  $name" >&2
    done
    $debug && echo "For selected collections:" >&2
    for p in $all_collections; do
      eval \$p_${p}_s || continue
      eval name=\"\$p_${p}_n\"
      echo "  $name ">&2
      list_files $p
    done
    for p in $all_lang_collections; do
      eval \$p_${p}_s || continue
      eval name=\"\$p_${p}_n\"
      $debug && echo "  $name ">&2
      list_files $p
    done
    $debug && echo "For selected packages:" >&2
    :>$work_dir/SPECIAL.list
    for p in $selected_packages; do
      list_package $p "SPECIAL"
    done
  ) 

  echo >&2
  echo "Now copying selected files" >&2
  $debug && echo " from the lists in $work_dir... " >&2
  for f in `ls $work_dir/*.*list`
  do
  $debug && echo "Copy files listed in $f" >&2
   $debug || $echon "`basename $f .list` " >&2
   sort -u $f > $f.uniq
   if test "x$TAROPT" = "x"; then
     (cd $CDDIR; $XARGS sh -c 'tar cf - $* \
        | (cd '"$TEXDIR"'; umask 0; tar xf -)' sh < $f.uniq )
   else
    (cd $CDDIR; $TARPROG -c -f - $TAROPT $f.uniq ) \
        | (cd $TEXDIR; umask 0; $TARPROG -x -f -)
   fi
 done 
 echo "Done copying." >&2
  test -n "$platform_subdir_strip_d" && rm -f $TEXDIR/bin/$platform_subdir_strip_d 2>/dev/null

  if test "x$TEXMF" != "x$TEXDIR/texmf"; then
    $echon "Fixing permissions in $TEXMF... " >&2
    chmod -R a+r,u+w,go-w $TEXMF 2>/dev/null
    rm -f $TEXDIR/texmf
    echo "Done fixing permissions." >&2
  fi
  $echon "Fixing permissions in $TEXDIR... " >&2
  chmod -R a+r,u+w,go-w $TEXDIR 2>/dev/null
  test -z "$VARTEXMF" ||
    chmod -R a+w $VARTEXMF ||
     warn "command 'chmod 1777 $VARTEXMF' failed"
  echo "Done fixing permissions." >&2
  if test "$opt_varfonts" = X; then
     $echon "$opt_varfonts_dir will be used for font creation... " >&2
  else
    $echon "Setting up directories for automatic font creation... " >&2
    find "$TEXMF/fonts/pk" "$TEXMF/fonts/tfm" "$TEXMF/fonts/source/jknappen" -type d \
      -print 2>/dev/null | $XARGS chmod 1777 2>/dev/null
  fi
  find "$TEXMF/fonts/source/jknappen" -type d -print 2>/dev/null | $XARGS chmod 1777 2>/dev/null
  echo "Done setting up directories." >&2
  $skip_systemstuff || maketex_setoptfonts
  $skip_systemstuff || TEXMFCNF__fix_fmtutil
  $skip_systemstuff || TEXMFCNF__fix_texmf
  pd=$this_platform_fn
  bindir=
  have_system=false
  echo Testing for $TEXDIR/bin/$pd/texconfig....  >&2
  if test -x $TEXDIR/bin/$pd/texconfig; then
    have_system=true
    bindir=$TEXDIR/bin/$pd
  elif test -x $TEXDIR/bin/texconfig; then
    have_system=true
    bindir=$TEXDIR/bin
  fi
  $skip_systemstuff && have_system=false
  echo "updating map files " >&2
  $skip_systemstuff || opt_do_symlinks
  $skip_systemstuff && echo "System updated successfully."  >&2
  $have_system && runsetup
  $have_system && echo You should make sure that $bindir  >&2
  $have_system && echo is added to your PATH for future sessions.  >&2
  $skip_systemstuff || greetings
  if $skip_systemstuff
  then
   echo Since you already had files in $TEXDIR/texmf/web2c
   echo We have not touched anything there. Please review your setup.
  fi
  exit
}

post_install_jobs ()
{
for f in `ls $work_dir/*.jobs`
do
  echo " do work items listed in $f" >&2
  for j in `sort -u $f`
  do
    command=`echo $j | sed 's/.\(.*\)=.*/\1/'`
    parameter=`echo $j | sed 's/.*=\(.*\)/\1/'`
    echo "   EXECUTE $command on $parameter" >&2
    case $command in
        addMap)
	    echo "Map $parameter" >> $VARTEXMF/web2c/updmap.cfg;;
        addMixedMap)
	    echo "MixedMap $parameter" >> $VARTEXMF/web2c/updmap.cfg;;
        addDvipsMap)
	    echo "p +$parameter" >> $VARTEXMF/dvips/config/config.ps;;
        addPdfTeXMap)
	    echo "map +$parameter" >> $VARTEXMF/pdftex/config/pdftex.cfg;;
        addDvipdfmMap)
	    test -f $VARTEXMF/dvipdfm/config/config && echo "f $parameter" >> $VARTEXMF/dvipdfm/config/config;
	    ;;
    esac
  done
done
}

runsetup()
{
  cat <<ENDC
I will now run "texconfig init" to initialize your system.
ENDC

PATH=$bindir:/bin:/usr/bin
export PATH
if test -f $VARTEXMF/tex/generic/config/language.dat
then
 echo Using language.dat from CD-ROM.  >&2
else
 echo making language.dat in $VARTEXMF/tex/generic/config  >&2
 echo from your language selections  >&2
 mkdirhier $VARTEXMF/tex/generic/config
 mkdirhier $VARTEXMF/fonts/map/dvipdfm/updmap
 mkdirhier $VARTEXMF/fonts/map/dvips/updmap
 mkdirhier $VARTEXMF/fonts/map/pdftex/updmap
cat $TEXDIR/texmf/tex/generic/config/language.us $TEXDIR/texmf/tex/generic/config/language.*.dat > $VARTEXMF/tex/generic/config/language.dat
fi
echo Wait while a simple format file is made. >&2
(cd $work_dir; $bindir/texconfig init tex > $VARTEXMF/texconfig.log)
echo The log of the texconfig run is in $VARTEXMF/texconfig.log >&2
echo ... now remaking filename database .... >&2
$bindir/mktexlsr
post_install_jobs
echo ...done >&2
echo Now we will make map files for dvips, pdftex etc >> $VARTEXMF/texconfig.log
$bindir/updmap --nohash --cnffile=$VARTEXMF/web2c/updmap.cfg  --dvipsoutputdir=$VARTEXMF/fonts/map/dvips/updmap --dvipdfmoutputdir=$VARTEXMF/fonts/map/dvipdfm/updmap --pdftexoutputdir=$VARTEXMF/fonts/map/pdftex/updmap
mktexlsr $VARTEXMF
cat <<ENDC
Now you are finished. 
For future configuration, edit files in $VARTEXMF
ENDC
}
################################################################
# main()
################################################################
unset TEXCONFIG
cd `dirname $0`
init
menu_main

