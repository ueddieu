#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
void set_fl(int,int);
char buf[1000000];
int
main(void)
{
	int ntowrite, nwrite;
	char *ptr;
	ntowrite = read(STDIN_FILENO, buf, sizeof(buf));
	fprintf(stderr, "read %d bytes\n", ntowrite);
//
	set_fl(STDOUT_FILENO, O_NONBLOCK);
	for (ptr = buf; ntowrite>0; ){
		errno = 0;
		nwrite = write(STDOUT_FILENO, ptr, ntowrite);
		fprintf (stderr, "nwrite = %d, errno = %d\n", nwrite, errno);
		if(nwrite > 0) {
			ptr += nwrite;
			ntowrite -= nwrite;
		}
	}
	exit(0);
} 

 
