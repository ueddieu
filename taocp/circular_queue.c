/**try to implement circular queue in sequencial memory allocation.*/
#include<stdlib.h>
#include<stdio.h>
#define M 10
#define Overflow 12
#define Underflow 13
int a[M];
int f=0;
int r=0;
int error_code=0;
void come(int value){
	int t=r;
	t++;//new value for r;
	if(t==M) t=0;
	if(t==f){
		printf("Overflow\n");
		error_code = Overflow;
	}else{
		a[r=t]=value;
	}
}
int go(){

	if(f==r){

		printf("Underflow\n");
		error_code = Underflow;
	}
	int t=f;
	t++;
	if(t==M) t=0;
	return a[f=t];
}
int main(char* args[]){
	int i=0;
	for(;i<M;i++){
		come(i);
	}
}
