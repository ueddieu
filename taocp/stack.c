#include <stdlib.h>
#include <stdio.h>
#define M 10
#define Underflow 12
#define Overflow 13
int a[M];
int t=-1;
int error_code=0;
int popup(){
	if(t==-1){
		printf("Underflow\n");
		error_code=Underflow;
		return Underflow;
	}else{
		return a[t--];
	}
}	
int pushdown(int value){
	if(t==M-1){
		printf("Overflow\n");
		error_code=Overflow;
		return Overflow;
	}else{
		a[++t]=value;
	}
}
int main(char* args[]){
	int i=0;
	for(; i< M; i++){
		pushdown(i);
	}
	pushdown(i);
}		
