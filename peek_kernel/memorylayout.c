#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
	char cmd[32];
	char pid[10];
	brk((void*)0x8051000);
/* cat /proc/self/maps does not work, in my PC, self will 
 * refer to cat process. so I use the explicit pid instead.
 */
	sprintf(cmd, "cat /proc/%d/maps\n",getpid());
	printf("command is: %s",cmd);
	fflush(stdout);
	system(cmd);
	return 0;
}
